#version 330

in vec3 vertex;
in vec3 vertexNormal;
in vec3 vertexColor;

out vec3 fragVertex;
out vec3 fragNormal;
out vec3 fragColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;


void main() {
	fragVertex = vertex;
	fragNormal = vertexNormal;
	fragColor  = vertexColor;


    gl_Position = proj * view * model * vec4(vertex, 1.0);
}