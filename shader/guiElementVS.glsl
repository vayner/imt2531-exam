#version 330

// Uniforms
uniform float zDepth;
uniform mat4 proj;

in vec2 vertex;
in vec2 vertexTexCoord;

out vec2 fragTexCoord;

void main() {
	fragTexCoord = vertexTexCoord;

    gl_Position = proj * vec4(vertex, zDepth, 1.0);
}