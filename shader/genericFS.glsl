#version 330

// in values
in vec3 fragVertex;
in vec3 fragNormal;
in vec3 fragColor;

// out color
out vec3 outColor;

// scene matrixes
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

// light information
uniform struct Light {
	vec3 position;
	vec3 intensities; //a.k.a the color of the light
} light;

uniform vec3 ambient;

void main() {

	//calculate normal in world coordinates
	mat3 normalMatrix = transpose(inverse(mat3(model)));
	vec3 normal = normalize(normalMatrix * fragNormal);

	//calculate the location of this fragment (pixel) in world coordinates
	vec3 fragPosition = vec3(model * vec4(fragVertex, 1));

	//calculate the vector from this pixels surface to the light source
	vec3 surfaceToLight = light.position - fragPosition;

	//calculate the cosine of the angle of incidence (brightness)
	float brightness = dot(normal, surfaceToLight) / (length(surfaceToLight) * length(normal));
	brightness = clamp(brightness, 0, 1);

	//calculate final color of the pixel, based on:
	// 1. The angle of incidence: brightness
	// 2. The color/intensities of the light: light.intensities
	outColor = clamp(brightness * light.intensities * fragColor + ambient * fragColor, 0.0, 1.0);
}