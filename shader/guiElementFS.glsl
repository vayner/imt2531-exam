#version 330

// Uniforms
uniform sampler2D baseTex;
uniform vec3 guiColor;

// in values
in vec2 fragTexCoord;

// out color
out vec4 outColor;

void main() {

	outColor = texture(baseTex, fragTexCoord) * vec4(guiColor, 1.0);
}