#version 330

// Uniforms
uniform float zDepth;
uniform mat4 proj;

in vec2 vertex;

void main() {

    gl_Position = proj * vec4(vertex, zDepth, 1.0);
}