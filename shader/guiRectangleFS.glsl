#version 330

// Uniforms
uniform vec3 guiColor;

// out color
out vec4 outColor;

void main() {

	outColor = vec4(guiColor, 1.0);
}