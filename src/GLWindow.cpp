#include "GLWindow.h"


GLWindow::GLWindow (const std::string &name, int width, int height, const unsigned flags) :
GLWindow (name, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, flags) {
}

GLWindow::GLWindow (const std::string &name, int x, int y, int width, int height, const unsigned flags) {
	this->window = SDL_CreateWindow (name.c_str (), x, y, width, height, flags | SDL_WINDOW_OPENGL);
	this->glContext = SDL_GL_CreateContext (this->window);
}

GLWindow::~GLWindow () {
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
}

//sets the spaw interval (sync) for this window if its active, has no effects if not active
int GLWindow::setSwapInterval (int interval) {
	if (isActiveContext ()) {
		return SDL_GL_SetSwapInterval (interval);
	}
	return 0;
}

// returns true if current context is the active one
bool GLWindow::isActiveContext () const {
	return (SDL_GL_GetCurrentContext () == this->glContext);
}

// sets the current window and context to the active one
int GLWindow::setActive () {
	return SDL_GL_MakeCurrent (this->window, this->glContext);
}