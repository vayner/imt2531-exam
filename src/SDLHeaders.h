/**
 * @brief   File including all SDL headers required by the program. This
 *          file is necessary to avoid cluttering the code-base with conditional
 *          includes whenever something from SDL is included.
 */

#pragma once
#define GLM_FORCE_RADIANS

#ifdef WIN32
    #include <gl/glew.h>    //necessary or glew WILL complain as it MUST be loaded before gl.h
    #include <SDL_opengl.h>
 	#include <SDL_thread.h>
	#include <SDL_mutex.h>
 	#include <SDL.h>
#else
    #include <GL/glew.h>
    #include <SDL2/SDL_opengl.h>
 	#include <SDL2/SDL_thread.h>
	#include <SDL2/SDL_mutex.h>
	#include <SDL2/SDL.h>
#endif
