#pragma once

#include <iostream>
#include <string>

#include "SDLHeaders.h"

namespace utility {

	std::string getString(const std::string &message, unsigned int min, unsigned int max, bool showLimit = true);
	std::string getString(const std::string &message, bool emptyStringAllowed = false);

	/**
	* Log an SDL error with some error message to the output stream of our choice
	* @param os The output stream to write the message too
	* @param msg The error message to write, format will be msg error: SDL_GetError()
	*/
	void logSDLError (std::ostream &os, const std::string &msg);

	std::string loadFileToString (const std::string &fileName);
	void loadShaderFromFile (GLuint shader, const std::string &fileName);
	GLuint loadCompileShader (const std::string &fileName, GLenum type);
	bool loadTexture (
		const std::string fileName,
		GLuint &backRefrenceID,
		GLenum image_format = GL_RGB,
		GLint internal_format = GL_RGB,
		GLint level = 0,
		GLint border = 0
	);


	bool GL_errorCheck (const std::string &place);
	void checkProgramLog (GLuint programID, int flag);
	void checkShaderLog(GLuint shaderID, int flag);
}