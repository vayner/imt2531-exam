#include "Camera.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>

Camera::Camera (bool orbit) : orbit(orbit), Basic3Ddata() {
}

void Camera::setLookAtPosition(glm::vec3 newLookAt) {
	this->lookAtPosition = newLookAt;
}

void Camera::relativeLookAtPosition(glm::vec3 addLookAt) {
	this->lookAtPosition += addLookAt;
}

glm::mat4 Camera::getMatrix () const {
	glm::mat4 viewMatrix;


	if (orbit) {

		viewMatrix = glm::translate (viewMatrix, position - lookAtPosition);
		viewMatrix *= glm::mat4_cast (rotation);
		viewMatrix = glm::translate (viewMatrix, lookAtPosition);
		viewMatrix = glm::scale (viewMatrix, scale);

	} else {

		/*
		viewMatrix = glm::lookAt(
			position,
			lookAtPosition,
			glm::vec3(0.0f, 1.0f, 0.0f)
		);*/

		viewMatrix = glm::mat4_cast (rotation) * glm::translate (viewMatrix, position);
	}

	return viewMatrix;
}

glm::vec3 Camera::getUp () const {
	return glm::vec3(glm::mat4_cast (rotation) * glm::vec4 (0.0f, 0.0f, 1.0f, 0.0f));
}

glm::vec3 Camera::getLookAtPosition () const {
	return this->lookAtPosition;
}