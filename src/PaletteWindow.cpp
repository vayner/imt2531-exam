#include "PaletteWindow.h"

#include <glm/glm.hpp>

#include "GuiElement.h"
#include "Utility.h"

//Screen attributes
const unsigned WINDOW_FLAGS = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS;

const int SCREEN_WIDTH = 256;
const int SCREEN_HEIGHT = 256;

PaletteWindow::PaletteWindow (ShaderHandler * shaderHandler) :
	shaderHandler (shaderHandler), GLWindow ("Color Palette", SCREEN_WIDTH, SCREEN_HEIGHT, WINDOW_FLAGS) {
}



bool PaletteWindow::init () {
	palette = new GuiElement (glm::vec2 (0.0f, 0.0f), glm::vec2 (SCREEN_WIDTH, SCREEN_HEIGHT), glm::vec2 (256.0f, 256.0f));
	palette->init (shaderHandler);
	palette->loadTexture ("textures/RGB_24bits_palette_color_chart.png");

	return !utility::GL_errorCheck ("PaletteWindow::init");
}

void PaletteWindow::onActivate () {

}



void PaletteWindow::update (double deltaTime) {

}

void PaletteWindow::draw () {
	glClearColor (0.0f, 0.0f, 0.0f, 1.0f);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	palette->updateUniformData ();
	palette->draw ();

	SDL_GL_SwapWindow (window);
}



void PaletteWindow::resizeWindow (int width, int height) {
	// WINDOW_FLAGS & SDL_WINDOW_RESIZABLE == 0
}



void PaletteWindow::keyboardDown (SDL_Keysym keyData) {

}

void PaletteWindow::keyboardUp (SDL_Keysym keyData) {

}

void PaletteWindow::mouseMove (const SDL_MouseMotionEvent &motion) {

}

void PaletteWindow::mouseDown (const SDL_MouseButtonEvent &button) {

}

void PaletteWindow::mouseUp (const SDL_MouseButtonEvent &button) {
}