#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class Basic3Ddata {
public:
	Basic3Ddata (glm::vec3 position, glm::quat rotation);
	Basic3Ddata (glm::vec3 position, glm::quat rotation, glm::vec3 scale);
	Basic3Ddata (glm::vec3 position, glm::quat rotation, glm::vec3 scale, glm::vec3 modelOffset);
	Basic3Ddata ();

	void setPosition (glm::vec3 newPosition);
	void relativPosition (glm::vec3 addPosition);
	glm::vec3 getPosition () const;

	void setOffset (glm::vec3 newOffset);
	void relativOffset (glm::vec3 addOffset);
	glm::vec3 getOffset () const;

	void setScale (glm::vec3 newScale);
	void relativScale (glm::vec3 addScale);
	glm::vec3 getScale () const;

	void setRotation (glm::quat newRotation);
	void relativRotation (glm::quat addRotation);
	glm::quat getRotation () const;

	

	glm::mat4 getMatrix ();

protected:

	glm::vec3 position;
	glm::vec3 offset;
	glm::vec3 scale;
	glm::quat rotation;

};