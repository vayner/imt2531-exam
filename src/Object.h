#pragma once

#include <string>
#include <vector>
#include <stack>

#include "SDLHeaders.h"
#include <glm/glm.hpp>

#include "Basic3Ddata.h"
#include "ShaderHandler.h"

struct VertexData {
	glm::vec3 vertex;
	glm::vec3 normal;
	glm::vec3 color;
};

struct TriangleData {
	glm::vec3 A;
	glm::vec3 B;
	glm::vec3 C;

	glm::vec3 center;
	glm::vec3 normal;
};

struct ColorUndoData {
	int positions[3];
	glm::vec3 oldColor;
	glm::vec3 newColor;
};

struct UndoPackage {
	std::vector<ColorUndoData> affected;
};

class Object : public Basic3Ddata {
public:
	Object (ShaderHandler * shaderHandler);
	Object (ShaderHandler * shaderHandler, const std::string &filePath);
    ~Object ();

	bool loadTxtFile (const std::string &filePath);
	bool isInitialised () { return initialised; }
    
	void undoColor ();
	void redoColor ();
	void resetColors ();
	void reloadColors ();

	void saveModel (bool newDefault = true);

	glm::vec3 getCenter ();
	void updateTriangleColorTrace (glm::vec3 start, glm::vec3 direction, glm::vec3 color);
	
	void updateUniformData (glm::mat4 proj, glm::mat4 view);
	void draw ();

private:

	bool initialised;
	std::vector<glm::ivec3> elements;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> colors;
	std::vector<glm::vec3> normals;
	
	std::vector<glm::vec3> originalColors;

	std::stack<UndoPackage> undoData;
	std::stack<UndoPackage> redoData;

	std::vector<VertexData> modelData;
	std::vector<TriangleData> triangleData;

	const ShaderProgram * shaderProgram;
	std::string fileName;

	GLint vertexAttribut, normalAttribut, colorAttribut;
	GLuint vao, vertexBuffer;

	GLint projMatrixUniform, viewMatrixUniform, modelMatrixUniform;
};
