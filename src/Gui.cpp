#include "Gui.h"

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>

Gui::Gui (glm::vec2 position, glm::vec2 screenSize) :
	Basic2Ddata (position), screenSize (screenSize) {

	glGenVertexArrays (1, &vao);
	glBindVertexArray (NULL);

	this->updateMatrix();
}

void Gui::updateMatrix () {
	proj = glm::ortho (0.0f, screenSize.x, 0.0f, screenSize.y);
}