#include "Utility.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "SDLHeaders.h"
#include "FreeImage.h"

// Queries the user for a string with spesific length
std::string utility::getString(const std::string &message, unsigned int min, unsigned int max, bool showLimit) {
	if (min > max) {
		std::cerr << "Ilegal operation, minimum length for getString cant be larger than maximum" << std::endl;
		return "";
	}

	std::string inn;
	std::string limitMessage = " | max char [" + std::to_string(min) + " - " + std::to_string(max) + " ]: ";

	do {

		std::cout << message << ((showLimit) ? (limitMessage) : "");
		std::getline(std::cin, inn);

	} while (inn.length() < min || inn.length() > max);

	return inn;
}

std::string utility::getString(const std::string &message, bool emptyStringAllowed /*= false*/) {

	std::string inn;

	do {

		std::cout << message;
		std::getline(std::cin, inn);

	} while (inn == "" && !emptyStringAllowed);

	return inn;
}

void utility::logSDLError (std::ostream &os, const std::string &msg) {
	os << msg << " error: " << SDL_GetError() << std::endl;
}

std::string utility::loadFileToString (const std::string &fileName) {
    std::ifstream fileStream(fileName);
    std::string content;

	if(fileStream.is_open() == false) {
		std::cout << "couldn't load file" << fileName << std::endl;
	}

    while( fileStream.good() ) {
        std::string line;
        std::getline(fileStream, line);
        content.append(line + "\n");
    }

    return content;
}

void utility::loadShaderFromFile (GLuint shader, const std::string &fileName) {
	std::string data = loadFileToString(fileName);

	GLchar const *shader_source = data.c_str();
    GLint const shader_length = data.size();

	glShaderSource(shader, 1, &shader_source, &shader_length);

	GL_errorCheck("loading shaders from file " + fileName);
}

GLuint utility::loadCompileShader (const std::string &fileName, GLenum type) {
	GLuint ref = glCreateShader(type);

	loadShaderFromFile(ref, fileName.c_str());
	glCompileShader(ref);

	    
	checkShaderLog(ref, GL_COMPILE_STATUS);


	return ref;
}

// returns true if there is a GL_ERROR
bool utility::GL_errorCheck (const std::string &place) {

	GLenum error = glGetError();
	bool isError = false;

	while (error != GL_NO_ERROR) {
		if ((error & GL_INVALID_ENUM) == 0) {
			isError = true;
		}

		printf( "OpenGL error at %s\n%s\n", place.c_str(), gluErrorString(error) );

		error = glGetError();
	};

	return isError;
}

void utility::checkProgramLog (GLuint programID, int flag) {
	GLint valid = 0;
	glGetProgramiv(programID, flag, &valid);

	if (valid == GL_FALSE) {
		GLint len = 0;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &len);

		std::vector<char> logArray;
		logArray.resize(len);

		glGetProgramInfoLog(programID, len, &len, &logArray.front());

		std::cerr << std::string(&logArray.front()) << std::endl;
	}
}

void utility::checkShaderLog(GLuint shaderID, int flag) {
	GLint valid = 0;
	glGetShaderiv(shaderID, flag, &valid);

	if (valid == GL_FALSE) {
		GLint len = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &len);

		std::vector<char> logArray;
		logArray.resize(len);

		glGetShaderInfoLog(shaderID, len, &len, &logArray.front());

		std::cerr << std::string(&logArray.front()) << std::endl;
	}
}

// returns true on sucsessful loading of image
bool utility::loadTexture (const std::string fileName, GLuint &backRefrenceID, GLenum image_format, GLint internal_format, GLint level, GLint border) {
	//image format
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	//pointer to the image, once loaded
	FIBITMAP *dib (0);
	//pointer to the image data
	BYTE* bits (0);
	//image width and height
	unsigned int width (0), height (0);

	//check the file signature and deduce its format
	fif = FreeImage_GetFileType (fileName.c_str(), 0);

	//if still unknown, try to guess the file format from the file extension
	if (fif == FIF_UNKNOWN) {
		fif = FreeImage_GetFIFFromFilename (fileName.c_str());
	}

	//if still unkown, return failure
	if (fif == FIF_UNKNOWN) {
		std::cerr << " loadTexture UNKNOWN FILE FORMAT: " << fileName << std::endl;
		return false;
	}
	/*
	if (fif == FIF_PNG && image_format == GL_RGBA) {
		unsigned char * image; //the raw pixels
		unsigned w, h;

		lodepng_decode32_file (&image, &w, &h, fileName.c_str ());

		//generate an OpenGL texture ID for this texture
		glGenTextures (1, &backRefrenceID);

		//bind to the new texture ID
		glBindTexture (GL_TEXTURE_2D, backRefrenceID);

		//store the texture data for OpenGL use
		glTexImage2D (GL_TEXTURE_2D, level, internal_format, w, h, border, image_format, GL_UNSIGNED_BYTE, image);

		return true;
	}
	*/

	//check that the plugin has reading capabilities and load the file
	if (FreeImage_FIFSupportsReading (fif))
		dib = FreeImage_Load (fif, fileName.c_str ());
	//if the image failed to load, return failure
	if (!dib) {
		std::cerr << " loadTexture FAILED TO LOAD: " << fileName << std::endl;
		return false;
	}
		

	//retrieve the image data
	bits = FreeImage_GetBits (dib);

	//get the image width and height
	width = FreeImage_GetWidth (dib);
	height = FreeImage_GetHeight (dib);

	//if this somehow one of these failed (they shouldn't), return failure
	if (bits == 0 || width == 0 || height == 0) {
		std::cerr << " loadTexture INVALID DATA: " << fileName << std::endl;
		return false;
	}
		
	//generate an OpenGL texture ID for this texture
	glGenTextures (1, &backRefrenceID);

	//bind to the new texture ID
	glBindTexture (GL_TEXTURE_2D, backRefrenceID);

	//store the texture data for OpenGL use
	glTexImage2D (GL_TEXTURE_2D, level, internal_format, width, height, border, image_format, GL_UNSIGNED_BYTE, bits);

	//Free FreeImage's copy of the data
	FreeImage_Unload (dib);

	//return success
	return !GL_errorCheck("loading of texture " + fileName);
}