#pragma once

#include "SDLHeaders.h"

#include <string>
#include <glm/glm.hpp>

#include "GuiRectangle.h"
#include "ShaderHandler.h"

struct gfxData {
	glm::vec2 vertex;
	glm::vec2 texCoord;
};


class GuiElement : public GuiRectangle {
public:

	GuiElement (glm::vec2 position, glm::vec2 screenSize, glm::vec2 size);
	GuiElement (glm::vec2 position, glm::vec2 screenSize, glm::vec2 size, glm::vec3 color);

	virtual void init (ShaderHandler * shaderSystem);
	virtual void loadTexture (const std::string &texture, GLenum image_format = GL_RGB, GLint internal_format = GL_RGB);
	
	virtual void updateUniformData ();
	virtual void draw ();

protected:

	void recalculateRectangle (glm::vec2 size, glm::vec2 position, glm::vec2 screenSize);


	bool texturized;

	GLuint textureID;
	int textureIndex;

	std::vector<glm::vec2> guiTexelCoord;
	std::vector<gfxData> guiData;

	GLint texelAttribut;
	GLint textureUniform;

private:

	bool first;
};