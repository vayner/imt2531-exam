
#include "WindowManager.h"
#include "MainWindow.h"
#include "PaletteWindow.h"
#include "Utility.h"

#include <iostream>

int main( int argc, char *argv[] ) {


	WindowManager manager;
	ShaderHandler shadeHandler;
	
	std::string modelPath = (argc == 2) ? argv[1] : utility::getString ("Model path: ");
	//utility::getString("Model path: ") // "models/teapotC.txt"

	MainWindow * mainWindow = new MainWindow ("Exam IMT2531 | 121240 | 12HBSPA |", modelPath, &shadeHandler); //teapotC
	//PaletteWindow * paletteWindow = new PaletteWindow (&shadeHandler);

	
	if (!manager.initGlew ()) {
		utility::logSDLError(std::cout, "Error creating main window" );
		system("pause");
		return EXIT_FAILURE;
	}

	// loading shaders
	shadeHandler.getShaderProgram ("shader/gui.shpr");
	shadeHandler.getShaderProgram ("shader/program.shpr");

	manager.addWindow (mainWindow);
	//manager.addWindow (paletteWindow);
	manager.run ();
	
	return EXIT_SUCCESS;
}

/*
	TODO

	* fix camera 100%
	* implement raycasting 100%
	* window intancing 30% works for 1 atleast
	* shader loading system 98% (working)
	* generalize objects 75% (problem with 2 or more related to textures)

*/