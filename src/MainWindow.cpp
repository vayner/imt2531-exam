#include "MainWindow.h"
#include "Utility.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

#include "SDLHeaders.h"

#include <iostream>
#include <fstream>
#include <string>

//Screen attributes
const unsigned WINDOW_FLAGS = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

const float SCREEN_WIDTH_DIV2 = SCREEN_WIDTH / 2;
const float SCREEN_HEIGHT_DIV2 = SCREEN_HEIGHT / 2;

const float SCREEN_ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT;
const float FOV = glm::pi<float>() / 3.0f;

const float NEAR_PLANE = 0.01f;
const float FAR_PLANE = 100.0f;

//The frame rate
const int FRAMES_PER_SECOND = 60;

MainWindow::MainWindow (const std::string &windowName, const std::string &modelPath, ShaderHandler * shaderHandler) :
	GLWindow (windowName, SCREEN_WIDTH, SCREEN_HEIGHT, WINDOW_FLAGS),
    valid(false),
	model(nullptr),
	shaderHandler (shaderHandler),
	modelPath (modelPath){
	
}

MainWindow::~MainWindow() {
    //free the objects memory
	if (model) {
		delete model;
		model = nullptr;
    }

	delete camera;
	//delete shaderHandler;

	camera = nullptr;
	shaderHandler = nullptr;
	programRef = nullptr;

    //cleanup
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
}

bool MainWindow::isValid() const {
    return this->valid;
}

bool MainWindow::init () {

	programRef = shaderHandler->getShaderProgram("shader/program.shpr");

	camera = new Camera(true);
	camera->setPosition(glm::vec3(3.0f, 0.0f, 3.0f));
	camera->setLookAtPosition (glm::vec3 (0.0f, 0.0f, 0.0f));

    //Initialize clear color
    glClearColor( 0.f, 0.f, 0.f, 1.f );
	
	glEnable(GL_DEPTH_TEST);                        // Enables Depth Testing
	glDepthFunc(GL_LEQUAL);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);		// default

	this->resizeWindow(SCREEN_WIDTH, SCREEN_HEIGHT);

	this->initLight ();
	this->initObjects (this->modelPath);

	this->valid = !utility::GL_errorCheck ("MainWindow::init");

	return this->valid;
}

void MainWindow::initLight () {
	light = Light();

	light.position = glm::vec3(-3.0f,3.0f,3.0f);
	light.intensities = glm::vec3(1.0f,1.0f,1.0f);
	ambient = glm::vec3 (0.07f, 0.07f, 0.07f);

	updateLight ();
}

void MainWindow::updateLight () {
	glUseProgram (programRef->getProgramID ());

	ambient = glm::clamp (ambient, 0.0f, 1.0f);
	glm::vec3 adjustedIntensity = glm::clamp (light.intensities, 0.0f, (lighting)? 1.0f : 0.0f);

	glUniform3f (programRef->getRefrences ("light.position"), light.position.x, light.position.y, light.position.z);
	glUniform3f (programRef->getRefrences ("light.intensities"), adjustedIntensity.x, adjustedIntensity.y, adjustedIntensity.z);
	glUniform3f (programRef->getRefrences ("ambient"), ambient.x, ambient.y, ambient.z);
}

void MainWindow::initObjects (const std::string &modelPath) {
	glm::vec2 screenArea = glm::vec2 (SCREEN_WIDTH, SCREEN_HEIGHT);

	model = new Object(shaderHandler, modelPath);

	palette = new Palette (glm::vec2 (0.0f, 0.0f), screenArea, glm::vec2 (256.0f, 198.0f), glm::vec2 (16.0f, 24.0f));
	palette->init (shaderHandler);

	colorDisplay = new GuiRectangle (glm::vec2 (0.0f, 222.0f), screenArea, glm::vec2 (16.0f, 24.0f), selectedColor);
	colorDisplay->init (shaderHandler);

	undoButton = new GuiElement (glm::vec2 (0.0f, SCREEN_HEIGHT - 64.0f), screenArea, glm::vec2 (64.0f, 64.0f));
	undoButton->init (shaderHandler);
	undoButton->loadTexture ("textures/undo.png", GL_BGR, GL_RGB);

	resetButton = new GuiElement (glm::vec2 (64.0f, SCREEN_HEIGHT - 64.0f), screenArea, glm::vec2 (64.0f, 64.0f));
	resetButton->init (shaderHandler);
	resetButton->loadTexture ("textures/reset.png", GL_BGR, GL_RGB);

	redoButton = new GuiElement (glm::vec2 (128.0f, SCREEN_HEIGHT - 64.0f), screenArea, glm::vec2 (64.0f, 64.0f));
	redoButton->init (shaderHandler);
	redoButton->loadTexture ("textures/redo.png", GL_BGR, GL_RGB);

	saveButton = new GuiElement (glm::vec2 (192.0f, SCREEN_HEIGHT - 32.0f), screenArea, glm::vec2 (32.0f, 32.0f));
	saveButton->init (shaderHandler);
	saveButton->loadTexture ("textures/save.png", GL_BGR, GL_RGB);

	drawInterface.push_back (palette);
	drawInterface.push_back (colorDisplay);
	drawInterface.push_back (undoButton);
	drawInterface.push_back (resetButton);
	drawInterface.push_back (redoButton);
	drawInterface.push_back (saveButton);
}

void MainWindow::resizeWindow (int width, int height) {
	glViewport(0,0, width, height);

	proj = glm::perspectiveFov (FOV, (float) SCREEN_WIDTH, (float) SCREEN_HEIGHT, NEAR_PLANE, FAR_PLANE);
}

void MainWindow::keyboardDown (SDL_Keysym keyData) {
	SDL_Keycode key = keyData.sym;
	if (key == SDLK_ESCAPE) { //Escape key
		SDL_Event quit;
		quit.type = SDL_QUIT;

		SDL_PushEvent (&quit);
	} else if (key == SDLK_LSHIFT || key == SDLK_RSHIFT) {
		orbiting = true;
	} else if (key == SDLK_LCTRL || key == SDLK_RCTRL) {
		zooming = true;
	} else if (key == SDLK_l) {
		lighting = !lighting;
		updateLight ();
	} else if (key == SDLK_KP_MINUS) {
		ambient -= 0.01;
		updateLight ();
	} else if (key == SDLK_KP_PLUS) {
		ambient += 0.01;
		updateLight ();
	} else if (key == SDLK_f) {
		ambient = glm::vec3 (1.0f, 1.0f, 1.0f);
		updateLight ();
	} else if (key == SDLK_n) {
		ambient = glm::vec3 (0.07f, 0.07f, 0.07f);
		updateLight ();
	}
}

void MainWindow::keyboardUp (SDL_Keysym keyData) {
	SDL_Keycode key = keyData.sym;
	if (key == SDLK_LSHIFT || key == SDLK_RSHIFT) {
		orbiting = false;
		drift = true;
	} else if (key == SDLK_LCTRL || key == SDLK_RCTRL) {
		zooming = false;
		drift = true;
	} else if (key == SDLK_SPACE) {
		spinnFigure = (!spinnFigure);
	} else if (key == SDLK_r) {
		model->resetColors();
	} else if (key == SDLK_s) {
		model->saveModel();
	} else if (key == SDLK_z) {
		model->undoColor();
	} else if (key == SDLK_a) {
		model->redoColor();
	} else if (key >= SDLK_1 && key <= SDLK_6) {
		cameraAngleX = cameraAngleY = 0.0f;

		switch (key) {
			case SDLK_1:
			camera->setRotation (glm::quat (glm::vec3 (0.0f, 0.0f, 0.0f)));
			break;

			case SDLK_2:
			camera->setRotation (glm::quat (glm::vec3 (0.0f, glm::half_pi<float> (), 0.0f)));
			break;

			case SDLK_3:
			camera->setRotation (glm::quat (glm::vec3 (0.0f, glm::pi<float> (), 0.0f)));
			break;

			case SDLK_4:
			camera->setRotation (glm::quat (glm::vec3 (0.0f, -glm::half_pi<float> (), 0.0f)));
			break;

			case SDLK_5:
			camera->setRotation (glm::quat (glm::vec3 (glm::half_pi<float> (), 0.0f, 0.0f)));
			break;

			case SDLK_6:
			camera->setRotation (glm::quat (glm::vec3 (-glm::half_pi<float> (), 0.0f, 0.0f)));
			break;
		}
	}
}

void MainWindow::mouseMove (const SDL_MouseMotionEvent &motion) {
	SDL_Keymod mod = SDL_GetModState ();
	if (mod & KMOD_CTRL) { //If control button is pressed down
		if(motion.state & SDL_BUTTON_LMASK) { //if left mouse button is currently pressed down
			cameraZoom *= (float)exp(0.003 * motion.yrel);
		}
	} else if (colorDraging) {

		selectedColor = palette->startDragColor (motion.x, motion.y);
		colorDisplay->setColor (selectedColor);

	} else if ((motion.state & SDL_BUTTON_LMASK) && (mod & KMOD_SHIFT)) { //if left mouse button is currently pressed down
		const float c = 0.01f;

		cameraAngleX = (double)motion.xrel * c;
		cameraAngleY = (double)motion.yrel * c;

		drift = false;
	}
	if (brushPainting) {
		if (motion.state & SDL_BUTTON_RMASK) {
			glm::vec3 pos = glm::unProject(glm::vec3(motion.x, SCREEN_HEIGHT - motion.y, 0.0f), camera->getMatrix() * model->getMatrix(), proj, glm::ivec4(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
			glm::vec3 dir = glm::normalize(glm::unProject(glm::vec3(motion.x, SCREEN_HEIGHT - motion.y, 1.0f), camera->getMatrix() * model->getMatrix(), proj, glm::ivec4(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)));
			model->updateTriangleColorTrace(pos, dir, selectedColor);
		} else {
			brushPainting = false;
		}
	}
}

void MainWindow::mouseDown (const SDL_MouseButtonEvent &click) {
	int x, y;
	SDL_GetMouseState (&x, &y);

	if (palette->hitTest (x, y)) {
		colorDraging = true;

		selectedColor = palette->startDragColor (x, y);
		colorDisplay->setColor (selectedColor);
	} else if (undoButton->hitTest (x, y)) {
		model->undoColor();
	} else if (redoButton->hitTest (x, y)) {
		model->redoColor();
	} else if (resetButton->hitTest (x, y)) {
		model->resetColors();
	} else if (saveButton->hitTest (x, y)) {
		model->saveModel();
	} else if (!(zooming || orbiting)) {
		if (click.button == SDL_BUTTON_RIGHT) {
			brushPainting = true;
		}


		//std::cout << "x: " << x << "   y: " << y << std::endl;;

		float nx = x;
		float ny = SCREEN_HEIGHT - y;

		glm::vec3 pos = glm::unProject(glm::vec3(nx, ny, 0.0f), camera->getMatrix() * model->getMatrix(), proj, glm::ivec4(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
		glm::vec3 dir = glm::normalize(glm::unProject(glm::vec3(nx, ny, 1.0f), camera->getMatrix() * model->getMatrix(), proj, glm::ivec4(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)));

		//std::cout << pos.x << " " << pos.y << " " << pos.z << std::endl;
		//std::cout << dir.x << " " << dir.y << " " << dir.z << std::endl << std::endl;

		model->updateTriangleColorTrace(pos, dir, selectedColor);
	}
}

void MainWindow::mouseUp (const SDL_MouseButtonEvent &click) {
	int x, y;
	SDL_GetMouseState (&x, &y);
	
	if (click.button & SDL_BUTTON_LMASK) {
		drift = true;
	} else if (click.button & SDL_BUTTON_RMASK) {
		brushPainting = false;
	}

	if (colorDraging) {
		palette->pickColor (x, y);
		colorDraging = false;
	}
}

//update is called once per frame
//deltaTime is the total time of the last frame
void MainWindow::update (double deltaTime) {
	const float fric = 0.95f;
	if (drift) {
		cameraAngleX *= fric;
		cameraAngleY *= fric;
	}

	undoButton->setColor(glm::vec3(1.0f, 1.0f, 1.0f) - selectedColor);
	redoButton->setColor(glm::vec3(1.0f, 1.0f, 1.0f) - selectedColor);


	drift = true;
	
	glUseProgram (programRef->getProgramID ());

	camera->relativRotation(glm::quat(glm::vec3(cameraAngleY, cameraAngleX, 0.0f)));
	camera->setPosition(glm::vec3(0.0f, 0.0f, -cameraZoom));

	if (!spinnFigure) {
		return;
	}

	// rorates model slowly based on elasped time
	glm::quat modelMatrix (glm::vec3 (0.0f, (float) deltaTime, 0.0f));
	model->relativRotation(modelMatrix);
}

void MainWindow::onActivate () {
	// unused
}

void MainWindow::draw () {

	utility::GL_errorCheck ("draw MainWindow start");

	// Clear the screen to black
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	utility::GL_errorCheck ("draw MainWindow settings");

	model->updateUniformData(proj, camera->getMatrix());
	model->draw();
    
	for (auto &gui : drawInterface) {
		gui->updateUniformData ();
		gui->draw ();
	}

	utility::GL_errorCheck ("draw MainWindow draw");

    // Swap buffers
	SDL_GL_SwapWindow(window);

	utility::GL_errorCheck ("draw MainWindow SDL_GL_SwapWindow");
}