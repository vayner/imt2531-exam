#pragma once

#include <map>
#include <iostream>

#include "GLWindow.h"
#include "Utility.h"

#include "SDLHeaders.h"

static bool isGlewInit = false;

class WindowManager {
public:

	WindowManager ();
	WindowManager (unsigned fpsCap);
	~WindowManager ();

	bool initGlew ();

	int addWindow (GLWindow * window);
	GLWindow * getWindow (unsigned ID) const;
	bool setActiveWindow (unsigned ID);

	void run ();

private:

	std::map<unsigned, GLWindow*> windows;


	unsigned fpsCap;
	bool limitFPS;
	bool running;
	bool quit;

	GLWindow * currentActive;
	GLWindow * loopActive;

};