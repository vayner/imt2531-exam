#pragma once

#include "GLWindow.h"
#include "ShaderHandler.h"
#include "GuiElement.h"

class PaletteWindow : public GLWindow {
public:

	PaletteWindow (ShaderHandler * shaderHandler);

	bool init ();
	
private:

	void onActivate ();

	void update (double deltaTime);
	void draw ();
	void resizeWindow (int width, int height);

	void keyboardDown (SDL_Keysym keyData);
	void keyboardUp (SDL_Keysym keyData);
	void mouseMove (const SDL_MouseMotionEvent &motion);
	void mouseDown (const SDL_MouseButtonEvent &button);
	void mouseUp (const SDL_MouseButtonEvent &button);


	ShaderHandler * shaderHandler;
	GuiElement * palette;
};