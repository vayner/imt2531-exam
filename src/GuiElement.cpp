#include "GuiElement.h"

#include <vector>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Utility.h"

GuiElement::GuiElement (glm::vec2 position, glm::vec2 screenSize, glm::vec2 size) :
	GuiElement (position, screenSize, size, glm::vec3 (1.0f, 1.0f, 1.0f)) {
}

GuiElement::GuiElement (glm::vec2 position, glm::vec2 screenSize, glm::vec2 size, glm::vec3 color) :
GuiRectangle (position, screenSize, size, color), first(true), texturized (false), textureIndex (1) {

	guiTexelCoord = std::vector<glm::vec2> ({
		glm::vec2 (0.0f, 0.0f),
		glm::vec2 (1.0f, 0.0f),
		glm::vec2 (1.0f, 1.0f),
		glm::vec2 (0.0f, 1.0f)
	});

	guiData.clear ();
	int index = 0;
	for (glm::vec2 &UV : guiTexelCoord) {
		gfxData data;
		data.vertex = guiVertex.at (index++);
		data.texCoord = UV;
		guiData.push_back (data);
	}
}

void GuiElement::init (ShaderHandler * shaderSystem) {
	glBindVertexArray (vao);

	shaderProgram = shaderSystem->getShaderProgram ("shader/guiElement.shpr");
	glUseProgram (shaderProgram->getProgramID ());


	vertexAttribut = shaderProgram->getRefrences ("vertex");
	texelAttribut = shaderProgram->getRefrences ("vertexTexCoord");

	colorUniform = shaderProgram->getRefrences ("guiColor");
	textureUniform = shaderProgram->getRefrences ("baseTex");
	zDepthUniform = shaderProgram->getRefrences ("zDepth");
	projectionMatrix = shaderProgram->getRefrences ("proj");

	glUniform1i (textureUniform, textureIndex);

	
	glGenBuffers (1, &vertexBuffer);
	glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData (GL_ARRAY_BUFFER, sizeof(gfxData) * guiData.size (), &guiData.front (), GL_STATIC_DRAW);

	// Specify the layout of the vertex data
	glEnableVertexAttribArray (vertexAttribut);
	glVertexAttribPointer (vertexAttribut, 2, GL_FLOAT, GL_FALSE, sizeof(gfxData), 0);

	// Specify the layout of the texel coordiantes data
	glEnableVertexAttribArray (texelAttribut);
	glVertexAttribPointer (texelAttribut, 2, GL_FLOAT, GL_FALSE, sizeof(gfxData), (const GLvoid*) (sizeof(glm::vec2)));

	glBindBuffer (GL_ARRAY_BUFFER, NULL);
	glBindVertexArray (NULL);
}

void GuiElement::loadTexture (const std::string &texturePath, GLenum image_format /*= GL_RGB*/, GLint internal_format /*= GL_RGB*/) {
	if (texturized) {
		return;
	}
	
	texturized = true;

	utility::GL_errorCheck ("GuiElement before loadTexture");

	textureID = 2;
	glBindVertexArray (vao);
	if (utility::loadTexture (texturePath, textureID, image_format, internal_format) == false) {
		std::cerr << " texture derp happend " << std::endl;
	}
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glBindVertexArray (NULL);
	utility::GL_errorCheck ("GuiElement after loadTexture");
}

void GuiElement::recalculateRectangle (glm::vec2 size, glm::vec2 position, glm::vec2 screenSize) {
	GuiRectangle::recalculateRectangle (size, position, screenSize);

	guiData.clear ();
	int index = 0;
	for (glm::vec2 &UV : guiTexelCoord) {
		gfxData data;
		data.vertex = guiVertex.at (index++);
		data.texCoord = UV;
		guiData.push_back (data);
	}
}


void GuiElement::updateUniformData () {
	glUseProgram (shaderProgram->getProgramID ());

	glUniformMatrix4fv (projectionMatrix, 1, GL_FALSE, glm::value_ptr (proj));
	glUniform3f (colorUniform, color.x, color.y, color.z);
	glUniform1f (zDepthUniform, depth);
}


void GuiElement::draw () {

	if (texturized == false) {
		return;
	}

	glBindVertexArray (vao);

	glUseProgram (shaderProgram->getProgramID());
	utility::GL_errorCheck ("draw GuiElement glUseProgram");

	glBindTexture (GL_TEXTURE_2D, textureID);
	utility::GL_errorCheck ("draw GuiElement glBindTexture");

	glActiveTexture (GL_TEXTURE0 + textureIndex);
	utility::GL_errorCheck ("draw GuiElement glActiveTexture");
	
	glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
	utility::GL_errorCheck ("draw GuiElement glBindBuffer");

	glDrawArrays (GL_TRIANGLE_FAN, 0, guiData.size ());


	glBindBuffer (GL_ARRAY_BUFFER, NULL);
	glUseProgram (NULL);
	glBindVertexArray (NULL);

	utility::GL_errorCheck ("draw GuiElement cleanup");
}