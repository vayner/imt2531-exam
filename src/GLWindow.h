#pragma once

#include "SDLHeaders.h"

#include <string>

// note: GLWindow's window will always have the flag SDL_WINDOW_OPENGL set to true
class GLWindow {
public:

	GLWindow (const std::string &name, int width, int height, const unsigned flags);
	GLWindow (const std::string &name, int x, int y, int width, int height, const unsigned flags);
	~GLWindow ();

	int setSwapInterval (int interval);
	bool isActiveContext () const;
	int setActive ();

	virtual bool init () = 0;
	virtual void onActivate () = 0;

	virtual void update (double deltaTime) = 0;
	virtual void draw () = 0;
	virtual void resizeWindow (int width, int height) = 0;

	virtual void keyboardDown (SDL_Keysym keyData) = 0;
	virtual void keyboardUp (SDL_Keysym keyData) = 0;
	virtual void mouseMove (const SDL_MouseMotionEvent &motion) = 0;
	virtual void mouseDown (const SDL_MouseButtonEvent &button) = 0;
	virtual void mouseUp (const SDL_MouseButtonEvent &button) = 0;

	SDL_Window * window;
	SDL_GLContext glContext;

};