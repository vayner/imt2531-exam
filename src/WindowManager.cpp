#include "WindowManager.h"

// if no FPS is set then 60 FPS is used
WindowManager::WindowManager () : 
	WindowManager(60) {
}

// if fpsCap == 0 then the FPS is unlimited
WindowManager::WindowManager (unsigned fpsCap) :
	fpsCap(fpsCap),
	running (false),
	quit(false) {

	this->limitFPS = (this->fpsCap > 0);

	//Initialize SDL
	if (SDL_Init (SDL_INIT_EVERYTHING) != 0) {
		utility::logSDLError (std::cout, "SDL_Init");
		return;
	}

	SDL_GL_SetAttribute (SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute (SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute (SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute (SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute (SDL_GL_CONTEXT_MINOR_VERSION, 3);
}

WindowManager::~WindowManager () {
	for (auto &w : this->windows) {
		delete w.second;
	}

	SDL_Quit ();
}

bool WindowManager::initGlew () {
	if (isGlewInit) {
		return true;
	} else {
		isGlewInit = true;
	}

	//Initialize glew
	glewExperimental = GL_TRUE;

	GLenum status = glewInit ();
	if (status != GLEW_OK) {
		std::cerr << "GLEW Error: " << glewGetErrorString (status) << "\n";
		return false;
	}

	//Print out some OpenGL details
	std::cout << ::glGetString (GL_VENDOR) << std::endl;
	std::cout << ::glGetString (GL_RENDERER) << std::endl;
	std::cout << ::glGetString (GL_VERSION) << std::endl;

	return true;
}


int WindowManager::addWindow (GLWindow * window) {
	Uint32 id = SDL_GetWindowID (window->window);

	this->windows[id] = window;

	setActiveWindow (id);

	return id;
}

GLWindow * WindowManager::getWindow (Uint32 ID) const {
	return this->windows.at(ID);
}

// tries to set the active window to that with the assosiated ID
//   returns false if no window with that ID exists, otherwise true
bool WindowManager::setActiveWindow (Uint32 ID) {
	try {
		this->currentActive = this->windows.at (ID);
	} catch (std::out_of_range ofr) {
		return false;
	}

	this->currentActive->setActive ();
	this->currentActive->onActivate ();

	return true;
}


void WindowManager::run () {
	for (auto &w : windows) {
		this->setActiveWindow (w.first);
		w.second->init ();
	}


	Uint32 prevFrameStartTime = 0;
	running = true;
	quit = false;


	//Wait for user exit
	while (running) {

		utility::GL_errorCheck ("run WindowManager start loop");

		loopActive = currentActive;

		//Used to regulate the frame rate
		Uint32 curFrameStartTime = SDL_GetTicks ();

		if (prevFrameStartTime == 0) {
			prevFrameStartTime = curFrameStartTime;
		}

		//Event handler
		SDL_Event * sdlEvent = new SDL_Event ();

		//While there are events to handle
		while (SDL_PollEvent (sdlEvent)) {
			switch (sdlEvent->type) {
				case SDL_QUIT:
					quit = true;
				break;

				case SDL_WINDOWEVENT_ENTER:
					loopActive = windows[sdlEvent->window.windowID];
				break;

				case SDL_KEYDOWN:
					loopActive->keyboardDown (sdlEvent->key.keysym);
				break;

				case SDL_KEYUP:
					loopActive->keyboardUp (sdlEvent->key.keysym);
				break;

				case SDL_WINDOWEVENT_RESIZED:
					loopActive->resizeWindow (sdlEvent->window.data1, sdlEvent->window.data1);
				break;

				case SDL_MOUSEMOTION:
					loopActive->mouseMove (sdlEvent->motion);
				break;

				case SDL_MOUSEBUTTONUP:
					loopActive->mouseUp (sdlEvent->button);
				break;

				case SDL_MOUSEBUTTONDOWN:
					loopActive->mouseDown (sdlEvent->button);
				break;
			}
		}

		utility::GL_errorCheck ("run WindowManager before update");

		//Run frame update
		loopActive->update (double (curFrameStartTime - prevFrameStartTime) / 1000);

		utility::GL_errorCheck ("run WindowManager before draw");

		//Render frame
		/*for (auto &w : windows) {
			setActiveWindow (w.first);
			w.second->draw ();
		}*/
		loopActive->draw ();

		//Regulate the frame rate
		Uint32 frameTime = SDL_GetTicks () - curFrameStartTime;
		if (frameTime < 1000 / fpsCap) {
			//Sleep the remaining frame time
			SDL_Delay ((1000 / fpsCap) - frameTime);
		}

		prevFrameStartTime = curFrameStartTime;
	
		// possibility for more exit handling code.
		// kinda meaningless for now
		if (quit) {
			running = false;
		}
	}
}