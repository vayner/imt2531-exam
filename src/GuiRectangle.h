#pragma once

#include <glm/glm.hpp>

#include "Gui.h"
#include "ShaderHandler.h"

class GuiRectangle : public Gui {
public:
	GuiRectangle (glm::vec2 position, glm::vec2 screenSize, glm::vec2 size, glm::vec3 color);
	~GuiRectangle ();

	virtual void init (ShaderHandler * shaderSystem);

	virtual bool hitTest (int x, int y);

	glm::vec3 getColor ();
	void setColor (glm::vec3 color);

	virtual void updateScreenSize (int width, int height);
	virtual void updateUniformData ();
	virtual void draw ();

protected:

	virtual void recalculateRectangle (glm::vec2 size);
	virtual void recalculateRectangle (glm::vec2 size, glm::vec2 position);
	virtual void recalculateRectangle (glm::vec2 size, glm::vec2 position, glm::vec2 screenSize);

	float depth;
	glm::vec3 color;
	glm::vec2 size;

	const ShaderProgram * shaderProgram;

	std::vector<glm::vec2> guiVertex;

	GLint vertexAttribut;
	GLint colorUniform, textureUniform, zDepthUniform, projectionMatrix;
	GLuint vertexBuffer;
};