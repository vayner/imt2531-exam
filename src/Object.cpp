#include "Object.h"

#include <vector>
#include <map>
#include <iostream>
#include <fstream>

#include <regex>

#include "Utility.h"
#include "SDLHeaders.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "triangleAlgorithm.h"

float distanceToLine (glm::vec3 linePoint, glm::vec3 lineDir, glm::vec3 point) {
	return glm::length (glm::cross (point - linePoint, glm::normalize(lineDir)));
}

Object::Object (ShaderHandler *shaderHandler) :
	initialised(false) {

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

	Basic3Ddata::position = glm::vec3(0.0f, 0.0f, 0.0f);
	Basic3Ddata::rotation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));

	shaderProgram = shaderHandler->getShaderProgram("shader/program.shpr");

	vertexAttribut = shaderProgram->getRefrences("vertex");
	normalAttribut = shaderProgram->getRefrences("vertexNormal");
	colorAttribut = shaderProgram->getRefrences("vertexColor");

	projMatrixUniform  = shaderProgram->getRefrences ("proj");
	viewMatrixUniform  = shaderProgram->getRefrences ("view");
	modelMatrixUniform = shaderProgram->getRefrences ("model");

	glBindVertexArray (NULL);
}

// combines creation and loading of model
Object::Object (ShaderHandler *shaderHandler, const std::string &filePath) :
	Object(shaderHandler) {

	initialised = this->loadTxtFile(filePath);
}


Object::~Object () {
	if (initialised) {
        glDeleteBuffers(1, &vertexBuffer);
		glDeleteVertexArrays(1, &vao);
    }
}

bool Object::loadTxtFile (const std::string &fileName) {
	if (initialised) {
		return false;
	}

	this->fileName = fileName;

	std::ifstream dataStream (fileName);

    if (dataStream.is_open() == false) {
        
		std::cout << "couldnt load file " << fileName << std::endl;
        return false;
    }

	int nVertices, nElements;

    dataStream >> nVertices >> nElements;
	
	// load points
    float vx, vy, vz;
    for (int i = 0; i < nVertices; i++) {
        dataStream >> vx >> vy >> vz;
		vertices.push_back(glm::vec3(vx, vy, vz));
    }

	// load indecis
    int e1, e2, e3;
    for (int i = 0; i < nElements; i++) {
        dataStream >> e1 >> e2 >> e3;
		elements.push_back(glm::ivec3(e1 - 1, e2 - 1, e3 - 1));
    }

	// load colors
	int cx, cy, cz;

	dataStream >> cx;

	if (dataStream.eof () == false) {
		dataStream >> cy >> cz;
		colors.push_back (glm::vec3 (cx, cy, cz) / 255.0f);

		for (int i = 1; i < nElements; i++) {
			dataStream >> cx >> cy >> cz;
			colors.push_back (glm::vec3 (cx, cy, cz) / 255.0f);
		}
	} else {
		for (int i = 0; i < nElements; i++) {
			colors.push_back (glm::vec3 (0.7f, 0.6f, 0.5f));
		}
	}


    dataStream.close();

	std::vector<std::vector<glm::vec3>> normalMapping;
	for (int i = 0; i < nVertices; i++) {
		normalMapping.push_back(std::vector<glm::vec3>());
	}

	// calculate arveaga nomals per vertex for all triangles it is part of triangle
	for (auto &i : elements) {
		
		// (y - x) cross (z - x) = normal of triangle
		glm::vec3 normal = glm::normalize(
			glm::cross(
				vertices.at(i.y) - vertices.at(i.x),
				vertices.at(i.z) - vertices.at(i.x)
			)
		);

		normalMapping.at(i.x).push_back(normal);
		normalMapping.at(i.y).push_back(normal);
		normalMapping.at(i.z).push_back(normal);
	}

	// calculate arvega for normals
	for (int i = 0; i < nVertices; i++) {
		glm::vec3 arv = glm::vec3(0.0f,0.0f,0.0f);

		for (auto &v : normalMapping.at(i)) {
			arv += v;
		}

		normals.push_back(glm::normalize(arv));
	}


	// reorders and merges data
	int index = 0;
	for (auto &e : elements) {
		VertexData dataX;

		dataX.vertex = vertices.at(e.x);
		dataX.normal = normals.at(e.x);
		dataX.color = colors.at(index);

		modelData.push_back(dataX);


		VertexData dataY;

		dataY.vertex = vertices.at(e.y);
		dataY.normal = normals.at(e.y);
		dataY.color = colors.at(index);

		modelData.push_back(dataY);


		VertexData dataZ;

		dataZ.vertex = vertices.at(e.z);
		dataZ.normal = normals.at(e.z);
		dataZ.color = colors.at(index);

		modelData.push_back(dataZ);


		TriangleData triData;
		triData.A = dataX.vertex;
		triData.B = dataY.vertex;
		triData.C = dataZ.vertex;

		triData.center = (triData.A + triData.B + triData.C) / 3.0f;
		triData.normal = glm::cross (triData.A - triData.B, triData.A - triData.C);

		triangleData.push_back (triData);

		++index;
	}

	for (auto &data : modelData) {
		originalColors.push_back (data.color);
	}

	glBindVertexArray (vao);

    // Create a Vertex Buffer Object and copy the vertex data to it
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData (GL_ARRAY_BUFFER, sizeof(VertexData) * modelData.size (), &modelData.front (), GL_DYNAMIC_DRAW);

	// Specify the layout of the vertex data
	glEnableVertexAttribArray(vertexAttribut);
	glVertexAttribPointer(vertexAttribut, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), 0);
	
    glEnableVertexAttribArray(normalAttribut);
	glVertexAttribPointer(normalAttribut, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const GLvoid*) sizeof(glm::vec3));
	
	glEnableVertexAttribArray(colorAttribut);
	glVertexAttribPointer(colorAttribut, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const GLvoid*) (2 * sizeof(glm::vec3)));


    glBindBuffer(GL_ARRAY_BUFFER, NULL);
	glBindVertexArray (NULL);

	if (utility::GL_errorCheck(" loadTxtFile ")) {
		return false;
	}

	if (fileName == "models/teapot.txt" || fileName == "models/teapotC.txt") {
		this->relativOffset (glm::vec3 (0.0f, -this->getCenter ().y / 2, 0.0f));
	}

    return true;  //Return true if succusfuly loaded
}

void Object::saveModel (bool newDefault) {
	std::fstream fileStream (fileName);

	if (!fileStream.is_open ()) {
		std::cerr << "Failed to open model file for color saving: " << fileName << std::endl;
        return;
	}

	int skipLines = 1 + elements.size () + vertices.size ();

	for (size_t i = 0; i < skipLines; i++) {
		fileStream.ignore (std::numeric_limits<std::streamsize>::max (), '\n');
	}
	
	if (newDefault) {
		originalColors.clear ();
	}

	
	for (auto &data : modelData) {
		if (newDefault) {
			originalColors.push_back (data.color);
		}

		fileStream
			<< (int) glm::roundEven (data.color.x * 255.0f) << ' '
			<< (int) glm::roundEven (data.color.y * 255.0f) << ' '
			<< (int) glm::roundEven (data.color.z * 255.0f) << '\n';
	}

	fileStream.flush ();
	fileStream.close ();
}

void Object::updateUniformData (glm::mat4 proj, glm::mat4 view) {
	glUseProgram (shaderProgram->getProgramID ());
	glUniformMatrix4fv (projMatrixUniform, 1, GL_FALSE, glm::value_ptr (proj));
	glUniformMatrix4fv (viewMatrixUniform, 1, GL_FALSE, glm::value_ptr (view));
	glUniformMatrix4fv (modelMatrixUniform, 1, GL_FALSE, glm::value_ptr (this->getMatrix ()));
}

void Object::draw () {
	if (!initialised) {
        return;
    }
	utility::GL_errorCheck ("draw start object");
	glBindVertexArray (vao);
	glUseProgram (shaderProgram->getProgramID ());

	utility::GL_errorCheck ("shader program enable");

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	
	glDrawArrays(GL_TRIANGLES, 0, modelData.size());
	
	glBindBuffer (GL_ARRAY_BUFFER, NULL);
	glUseProgram (NULL);
	glBindVertexArray (NULL);

	utility::GL_errorCheck ("draw end");
}

glm::vec3 Object::getCenter () {
	glm::vec3 arv;
	bool first = true;

	for (auto &v : vertices) {
		arv += v;

		if (first) {
			first = false;
			continue;
		}

		arv /= 2.0f;
	}

	return arv;
}

void Object::undoColor () {
	if (undoData.size () == 0) {
		return;
	}

	UndoPackage data = undoData.top ();

	for (auto &info : data.affected) {

		modelData.at (info.positions[0]).color = info.oldColor;
		modelData.at (info.positions[1]).color = info.oldColor;
		modelData.at (info.positions[2]).color = info.oldColor;

	}

	undoData.pop ();
	redoData.push (data);

	this->reloadColors ();
}

void Object::redoColor () {
	if (redoData.size () == 0) {
		return;
	}

	UndoPackage data = redoData.top ();

	for (auto &info : data.affected) {

		modelData.at (info.positions[0]).color = info.newColor;
		modelData.at (info.positions[1]).color = info.newColor;
		modelData.at (info.positions[2]).color = info.newColor;

	}

	redoData.pop ();
	undoData.push (data);

	this->reloadColors ();
}

void Object::resetColors () {
	UndoPackage undoPackage;

	int index = 0;
	for (auto &data : modelData) {
		if (index % 3 == 0) {
			ColorUndoData info;

			info.positions[0] = index + 0;
			info.positions[1] = index + 1;
			info.positions[2] = index + 2;

			info.oldColor = data.color;
			data.color = originalColors.at (index);
			info.newColor = data.color;

			undoPackage.affected.push_back (info);
		} else {
			data.color = originalColors.at (index);
		}

		++index;
	}

	undoData.push (undoPackage);

	this->reloadColors ();
}

void Object::reloadColors () {
	glBindVertexArray (vao);
	glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData (GL_ARRAY_BUFFER, sizeof(VertexData) * modelData.size (), &modelData.front (), GL_DYNAMIC_DRAW);
	glBindVertexArray (NULL);
}

void Object::updateTriangleColorTrace (glm::vec3 start, glm::vec3 direction, glm::vec3 color) {
	
	TA::Ray r;
	r.P0 = start;
	r.P1 = start + direction;

	int index = 0;
	int count = 0;
	int selected = -1;
	int result;

	float minDist = 9001.0f;

	// Iterate through the triangles in the figure
	for (TriangleData &td : triangleData) {

		TA::Triangle t;
		t.V0 = td.A;
		t.V1 = td.B;
		t.V2 = td.C;

		TA::Point p;

		result = TA::intersect3D_RayTriangle (r, t, p);

		if (result == 1) {
			if (selected == -1 || glm::length (p - start) < minDist) {
				minDist = glm::length (p - start);
				selected = index;
			}
			++count;
		}
		++index;
	}

	if (selected != -1) {
		UndoPackage data;
		ColorUndoData info;
		info.newColor = color;
		info.oldColor = modelData.at (selected * 3).color;

		if (glm::length(info.newColor - info.oldColor) < glm::epsilon<float>()) {
			return;
		}

		for (int i = 0; i < 3; i++) {
			info.positions[i] = selected * 3 + i;
			modelData.at (selected * 3 + i).color = color;
		}

		redoData = std::stack<UndoPackage> ();

		data.affected.push_back (info);
		undoData.push (data);

		this->reloadColors ();

	}
}