#pragma once

#include <glm/glm.hpp>
#include <vector>

#include "GuiElement.h"
#include "GuiElement.h"

class Palette : public GuiElement {
public:
	Palette (glm::vec2 position, glm::vec2 screenSize, glm::vec2 size, glm::vec2 lastPickSize);
	Palette (glm::vec2 position, glm::vec2 screenSize, glm::vec2 size);
	~Palette ();

	// presumes x & y is in screen space, returns NULL if x & y is outside of palette
	glm::vec3 pickColor (int x, int y);
	glm::vec3 startDragColor (int x, int y);

	bool hitTest (int x, int y);

	void init (ShaderHandler * shaderSystem);
	void draw ();

private:

	glm::vec2 lastPickSize;
	bool lastHistory = false;
	bool isDragSelecting = false;

	std::vector<GuiRectangle*> lastPicks;

};