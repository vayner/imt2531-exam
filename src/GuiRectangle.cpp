#include "GuiRectangle.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>

#include "Utility.h"

GuiRectangle::GuiRectangle (glm::vec2 position, glm::vec2 screenSize, glm::vec2 size, glm::vec3 color) :
Gui (position, screenSize), size (size), color (color), depth (1.0f) {
	this->recalculateRectangle (size, position, screenSize);
}

GuiRectangle::~GuiRectangle () {
	glDeleteBuffers (1, &vertexBuffer);
	glDeleteVertexArrays (1, &vao);
}


void GuiRectangle::init (ShaderHandler * shaderSystem) {

	shaderProgram = shaderSystem->getShaderProgram ("shader/guiRectangle.shpr");

	vertexAttribut = shaderProgram->getRefrences ("vertex");

	colorUniform = shaderProgram->getRefrences ("guiColor");
	zDepthUniform = shaderProgram->getRefrences ("zDepth");
	projectionMatrix = shaderProgram->getRefrences ("proj");

	glUseProgram (shaderProgram->getProgramID ());

	glBindVertexArray (vao);
	glGenBuffers (1, &vertexBuffer);
	glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData (GL_ARRAY_BUFFER, sizeof(glm::vec2) * guiVertex.size (), &guiVertex.front (), GL_STATIC_DRAW);

	// Specify the layout of the vertex data
	glEnableVertexAttribArray (vertexAttribut);
	glVertexAttribPointer (vertexAttribut, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), 0);

	glBindBuffer (GL_ARRAY_BUFFER, NULL);
	glBindVertexArray (NULL);
}

bool GuiRectangle::hitTest (int x, int y) {
	return (
		x > position.x &&
		x < position.x + size.x &&
		y > position.y &&
		y < position.y + size.y
		);
}

void GuiRectangle::updateScreenSize (int width, int height) {

	guiVertex.clear ();

	this->recalculateRectangle (glm::vec2 (width, height));

	glBindVertexArray (vao);
	glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData (GL_ARRAY_BUFFER, sizeof(glm::vec2) * guiVertex.size (), &guiVertex.front (), GL_STATIC_DRAW);
	glBindBuffer (GL_ARRAY_BUFFER, NULL);
	glBindVertexArray (NULL);
}

glm::vec3 GuiRectangle::getColor () {
	return this->color;
}

void GuiRectangle::setColor (glm::vec3 color) {
	this->color = color;
}

void GuiRectangle::recalculateRectangle (glm::vec2 size) {
	this->recalculateRectangle (size, this->position, this->screenSize);
}
void GuiRectangle::recalculateRectangle (glm::vec2 size, glm::vec2 position) {
	this->recalculateRectangle (size, position, this->screenSize);
}

void GuiRectangle::recalculateRectangle (glm::vec2 size, glm::vec2 position, glm::vec2 screenSize) {

	this->size = size;
	this->position = position;
	this->screenSize = screenSize;

	guiVertex.clear ();

	glm::vec2 lowerLeft;
	glm::vec2 lowerRight;
	glm::vec2 upperRight;
	glm::vec2 upperLeft;

	lowerLeft  = glm::vec2 (position.x			,	screenSize.y - (position.y + size.y));
	lowerRight = glm::vec2 (position.x + size.x	,	screenSize.y - (position.y + size.y));
	upperRight = glm::vec2 (position.x + size.x	,	screenSize.y - position.y);
	upperLeft  = glm::vec2 (position.x			,	screenSize.y - position.y);

	guiVertex.push_back (lowerLeft);
	guiVertex.push_back (lowerRight);
	guiVertex.push_back (upperRight);
	guiVertex.push_back (upperLeft);

}

void GuiRectangle::updateUniformData () {
	glUseProgram (shaderProgram->getProgramID ());

	glUniformMatrix4fv (projectionMatrix, 1, GL_FALSE, glm::value_ptr (proj));
	glUniform3f (colorUniform, color.x, color.y, color.z);
	glUniform1f (zDepthUniform, depth);
}

void GuiRectangle::draw () {

	glBindVertexArray (vao);

	glUseProgram (shaderProgram->getProgramID ());
	utility::GL_errorCheck ("draw GuiRectangle glUseProgram");

	glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
	utility::GL_errorCheck ("draw GuiRectangle glBindBuffer");

	glDrawArrays (GL_TRIANGLE_FAN, 0, guiVertex.size ());

	glBindBuffer (GL_ARRAY_BUFFER, NULL);
	glUseProgram (NULL);
	glBindVertexArray (NULL);

	utility::GL_errorCheck ("draw GuiRectangle cleanup");
}