#pragma once

#include "SDLHeaders.h"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "Basic3Ddata.h"

class Camera : public Basic3Ddata {
public:
	Camera (bool orbit);

	glm::mat4 getMatrix () const;
	glm::vec3 getUp () const;
	glm::vec3 getLookAtPosition () const;

	void setLookAtPosition (glm::vec3 newLookAt);
	void relativeLookAtPosition(glm::vec3 addLookAt);

private:

	bool orbit;
	glm::vec3 lookAtPosition;

};