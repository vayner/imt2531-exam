#pragma once
#include <glm/glm.hpp>
// Copyright 2001 softSurfer, 2012 Dan Sunday
// This code may be freely used and modified for any purpose
// providing that this copyright notice is included with it.
// SoftSurfer makes no warranty for this code, and cannot be held
// liable for any real or imagined damage resulting from its use.
// Users of this code must verify correctness for their application.

// http://geomalgorithms.com/a06-_intersect-2.html

// Editet by Tellef Møllerup Åmdal for glm


// Assume that classes are already given for the objects:
//    Point and Vector with
//        coordinates {float x, y, z;}
//        operators for:
//            == to test  equality
//            != to test  inequality
//            (Vector)0 =  (0,0,0)         (null vector)
//            Point   = Point ± Vector
//            Vector =  Point - Point
//            Vector =  Scalar * Vector    (scalar product)
//            Vector =  Vector * Vector    (cross product)
//    Line and Ray and Segment with defining  points {Point P0, P1;}
//        (a Line is infinite, Rays and  Segments start at P0)
//        (a Ray extends beyond P1, but a  Segment ends at P1)
//    Plane with a point and a normal {Point V0; Vector  n;}
//    Triangle with defining vertices {Point V0, V1, V2;}
//    Polyline and Polygon with n vertices {int n;  Point *V;}
//        (a Polygon has V[n]=V[0])
//===================================================================
 
namespace TA {

#define SMALL_NUM   0.00000001 // anything that avoids division overflow
	// dot product (3D) which allows vector operations in arguments
	//#define dot(u,v)   ((u).x * (v).x + (u).y * (v).y + (u).z * (v).z)
	//defined via glm


	typedef glm::vec3 Vector;
	typedef glm::vec3 Point;


	struct Ray {
		glm::vec3 P0;
		glm::vec3 P1;
	};

	struct Triangle {
		glm::vec3 V0;
		glm::vec3 V1;
		glm::vec3 V2;
	};

	// intersect3D_RayTriangle(): find the 3D intersection of a ray with a triangle
	//    Input:  a Ray R, a Triangle T and a Point I for return data
	//    Output:  I = intersection point (when it exists)
	//    Return: -1 = triangle is degenerate (a segment or point)
	//             0 =  disjoint (no intersect)
	//             1 =  intersect in unique point I1
	//             2 =  are in the same plane
	int intersect3D_RayTriangle (Ray R, Triangle T, Point &I) {

		Vector    u, v, n;              // triangle vectors
		Vector    dir, w0, w;           // ray vectors
		float     r, a, b;              // params to calc ray-plane intersect

		// get triangle edge vectors and plane normal
		u = T.V1 - T.V0;
		v = T.V2 - T.V0;
		n = glm::cross (u, v);          // cross product
		if (n == Vector ()) {           // triangle is degenerate
			return -1;                  // do not deal with this case
		}

		dir = R.P1 - R.P0;              // ray direction vector
		w0 = R.P0 - T.V0;
		a = -glm::dot (n, w0);
		b = glm::dot (n, dir);
		if (glm::abs (b) < SMALL_NUM) { // ray is  parallel to triangle plane
	             
			// true: ray lies in triangle plane 
			// false: ray disjoint from plane

			return (a == 0) ? 2 : 0;
		}

		// get intersect point of ray with triangle plane
		r = a / b;
		if (r < 0.0) {                  // ray goes away from triangle
			return 0;					// => no intersect
		}
		// for a segment, also test if (r > 1.0) => no intersect

		I = R.P0 + r * dir;             // intersect point of ray and plane

		// is I inside T?
		float uu, uv, vv, wu, wv, D;

		uu = glm::dot (u, u);
		uv = glm::dot (u, v);
		vv = glm::dot (v, v);

		w = I - T.V0;
		wu = glm::dot (w, u);
		wv = glm::dot (w, v);

		D = uv * uv - uu * vv;

		// get and test parametric coords
		float s, t;

		s = (uv * wv - vv * wu) / D;
		if (s < 0.0 || s > 1.0) {       // I is outside T
			return 0;
		}

		t = (uv * wu - uu * wv) / D;
		if (t < 0.0 || (s + t) > 1.0) { // I is outside T
			return 0;
		}

		return 1;                       // I is in T
	}

}