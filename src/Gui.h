#pragma once

#include <glm/glm.hpp>

#include "Basic2Ddata.h"
#include "ShaderHandler.h"

class Gui : public Basic2Ddata {
public:
	Gui (glm::vec2 position, glm::vec2 screenSize);

	void updateMatrix ();

	virtual void init (ShaderHandler * shaderSystem) = 0;
	virtual void updateScreenSize (int width, int height) = 0;

	virtual void updateUniformData () = 0;
	virtual void draw () = 0;


protected:

	glm::vec2 screenSize;
	glm::mat4 proj;
	GLuint vao;
};