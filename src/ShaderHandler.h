#pragma once

#include <map>
#include <vector>
#include <string>

#include "SDLHeaders.h"

enum class SHADERTYPE : GLenum { VERTEX = GL_VERTEX_SHADER, FRAGMENT = GL_FRAGMENT_SHADER };

// contains information needed for a complete shader program for opengl
class ShaderProgram {
public:
	ShaderProgram (GLuint programID);
	~ShaderProgram ();

	// get data
	GLuint getVertexShader () const;
	GLuint getFragmentShader () const;
	GLuint getProgramID () const;
	GLint getRefrences (const std::string &name) const;

	// set some values
	void setShader (GLint shaderID, SHADERTYPE type);
	void createAttributeRefrence(const std::string &name);
	void createUniformRefrence(const std::string &name);

private:

	SDL_sem * threadLock;

	GLuint vertexShader, fragmentShader, programID;
	std::map<std::string, GLint> refrences;
};

// contains information for a single shader for opengl
class Shader {
public:
	Shader (GLint shaderID, SHADERTYPE type);
	~Shader ();

	GLuint getShaderID () const;
	SHADERTYPE getType () const;

private:

	SDL_sem * threadLock;

	GLuint shaderID;
	SHADERTYPE type;
};

// 
class ShaderHandler {
public:


	ShaderHandler ();
	~ShaderHandler ();

	ShaderProgram * getShaderProgram (GLuint ID) const;
	ShaderProgram * getShaderProgram (const std::string &path);

private:

	ShaderProgram * loadShaderProgram (const std::string &path);
	Shader * loadShader (const std::string &path, SHADERTYPE type);

	SDL_sem * threadLock;

	std::map<std::string, GLuint>  fileToID;
	std::map<GLuint, ShaderProgram*> IDtoShaderProgram;
	std::map<GLuint, Shader*> IDtoShader;

	std::vector<ShaderProgram*> shaderPrograms;
	std::vector<Shader*> shaders;
};