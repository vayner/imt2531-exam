#include "Basic2Ddata.h"

Basic2Ddata::Basic2Ddata () : 
	Basic2Ddata (glm::vec2()) {
}

Basic2Ddata::Basic2Ddata (glm::vec2 position) :
	position(position) {
}

void Basic2Ddata::setPosition (glm::vec2 newPosition) {
	this->position = newPosition;
}

void Basic2Ddata::relativPosition (glm::vec2 addPosition) {
	this->position += addPosition;
}