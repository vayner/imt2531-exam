#pragma once

#include <glm/glm.hpp>

class Basic2Ddata {
public:

	Basic2Ddata (glm::vec2 position);
	Basic2Ddata ();

	void setPosition (glm::vec2 newPosition);
	void relativPosition (glm::vec2 addPosition);

protected:

	glm::vec2 position;

};