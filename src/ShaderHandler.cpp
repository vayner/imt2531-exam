#include "ShaderHandler.h"

#include <iostream>
#include <fstream>
#include <string>
#include <map>

#include "Utility.h"

// FILE GLOBAL STUFF

enum class KEYWORDS { START, PRE, POST, END, VS, FS, BFDL, GAL, GUL, UNDEFINED };
enum class STAGE : int {NONE = 0, START = 1, PRE = 2, POST = 3, END = 4, ERROR = 1337};

std::map<std::string, KEYWORDS> keywordAssosiation {
	{ ":START:",	KEYWORDS::START },
	{ ":PRE:",		KEYWORDS::PRE	},
	{ ":POST:",		KEYWORDS::POST	},
	{ ":END:",		KEYWORDS::END	},
	{ ":VS:",		KEYWORDS::VS	},
	{ ":FS:",		KEYWORDS::FS	},
	{ ":BFDL:",		KEYWORDS::BFDL	},
	{ ":GAL:",		KEYWORDS::GAL	},
	{ ":GUL:",		KEYWORDS::GUL	}
};

std::map<STAGE, std::string> stageNames {
	{ STAGE::NONE,	"not at keyword (NONE)"		},
	{ STAGE::START, ":START:"					},
	{ STAGE::PRE,	":PRE:"						},
	{ STAGE::POST,	":POST:"					},
	{ STAGE::END,	":END:"						},
	{ STAGE::ERROR, "not at keyword (ERROR)"	}
};

KEYWORDS parseKeyword(const std::string &word) {
	try {
		return keywordAssosiation.at(word);
	} catch (std::out_of_range ofr) {
		std::cerr << "ShaderHandler error, this word makes no sense   " << word << std::endl;
		return KEYWORDS::UNDEFINED;
	}
}

// checks if stage is a certain STAGE, sets stage to STAGE::ERROR if not
//  returns true if stage == expects otherwise false
bool checkStage (STAGE &stage, STAGE expects) {
	if (stage != expects) {
		std::cerr << "Shader program keyword error\nexpected  " << stageNames[expects] << "    got  " << stageNames[stage] << std::endl;
		stage = STAGE::ERROR;
		return false;
	} else {
		return true;
	}
}

STAGE checkAndIncrementStage (STAGE stage, STAGE checkFor, STAGE setTo) {
	if (stage != checkFor) {
		std::cerr << "Shader program keyword error\nexpected  " << stageNames[checkFor] << "    got  " << stageNames[stage] << std::endl;
		return STAGE::ERROR;
	} else {
		return setTo;
	}
}

// ---- ShaderProgram ----

ShaderProgram::ShaderProgram(GLuint programID) :
	programID(programID),
	vertexShader(-1),
	fragmentShader(-1) {
	threadLock = SDL_CreateSemaphore (1);
}

ShaderProgram::~ShaderProgram () {
	glDeleteProgram(programID);
	SDL_DestroySemaphore (threadLock);
}


GLuint ShaderProgram::getVertexShader() const{
	return this->vertexShader;
}

GLuint ShaderProgram::getFragmentShader() const{
	return this->fragmentShader;
}

GLuint ShaderProgram::getProgramID() const{
	return this->programID;
}

GLint ShaderProgram::getRefrences(const std::string &name) const {
	return this->refrences.at(name);
}


void ShaderProgram::setShader(GLint shaderID, SHADERTYPE type) {
	SDL_SemWait (threadLock);

	glAttachShader(this->programID, shaderID);

	if (type == SHADERTYPE::VERTEX && this->vertexShader == -1) {
		this->vertexShader = shaderID;
	} else if (type == SHADERTYPE::FRAGMENT && this->fragmentShader == -1) {
		this->fragmentShader = shaderID;
	}

	SDL_SemPost (threadLock);
}

void ShaderProgram::createAttributeRefrence(const std::string &name) {
	SDL_SemWait (threadLock);

	this->refrences[name] = glGetAttribLocation(this->getProgramID(), name.c_str());
	if (this->refrences[name] == -1) {
		std::cerr << "Could not get refrence to Attrib " << name << std::endl;
	}

	SDL_SemPost (threadLock);
}

void ShaderProgram::createUniformRefrence(const std::string &name) {
	SDL_SemWait (threadLock);

	this->refrences[name] = glGetUniformLocation(this->getProgramID(), name.c_str());
	if (this->refrences[name] == -1) {
		std::cerr << "Could not get refrence to uniform " << name << std::endl;
	}

	SDL_SemPost (threadLock);
}

// ---- Shader ----

Shader::Shader (GLint shaderID, SHADERTYPE type) :
	shaderID(shaderID),
	type(type) {
	threadLock = SDL_CreateSemaphore (1);
}

Shader::~Shader () {
	glDeleteShader(shaderID);
	SDL_DestroySemaphore (threadLock);
}

GLuint Shader::getShaderID () const {
	return this->shaderID;
}

SHADERTYPE Shader::getType () const {
	return this->type;
}

// ---- ShaderHandler ----

ShaderHandler::ShaderHandler () {
	threadLock = SDL_CreateSemaphore (1);
}

ShaderHandler::~ShaderHandler () {
	for (auto &s : shaders) {
		delete s;
	}

	for (auto &s : shaderPrograms) {
		delete s;
	}

	SDL_DestroySemaphore (threadLock);
}

ShaderProgram * ShaderHandler::getShaderProgram (GLuint ID) const {
	SDL_SemWait (threadLock);
	ShaderProgram * returns;

	try {
		returns =  IDtoShaderProgram.at (ID);
	} catch (std::out_of_range ofr) {
		returns =  NULL;
	}

	SDL_SemPost (threadLock);
	return returns;
}

ShaderProgram * ShaderHandler::getShaderProgram (const std::string &path) {
	SDL_SemWait (threadLock);
	ShaderProgram * returns;

	try {
		returns = IDtoShaderProgram.at (fileToID.at (path));
	} catch (std::out_of_range ofr) {
		returns = loadShaderProgram (path);
	}

	SDL_SemPost (threadLock);
	return returns;
}

// retuns NULL if shader program files keyword oreder is incorrect;
ShaderProgram * ShaderHandler::loadShaderProgram (const std::string &path) {
	std::ifstream fileStream(path);
	std::string line;
	std::string data;
	KEYWORDS keyword = KEYWORDS::START;
	STAGE stage = STAGE::NONE;

	if (fileStream.is_open() == false || fileStream.fail()) {
		return NULL;
	}

	ShaderProgram * program = new ShaderProgram(glCreateProgram());

	std::getline(fileStream, line);
	keyword = parseKeyword(line.substr(0, line.find_last_of(":") + 1));
	data = (line.find_last_of(":") + 2 < line.length())? line.substr(line.find_last_of(":") + 2) : "";

	while (fileStream.eof() == false && fileStream.good()) {
		switch (keyword) {
			case KEYWORDS::START:
				stage = checkAndIncrementStage(stage, STAGE::NONE, STAGE::START);
			break;

			case KEYWORDS::PRE:
				stage = checkAndIncrementStage(stage, STAGE::START, STAGE::PRE);
			break;

			case KEYWORDS::POST:
				stage = checkAndIncrementStage(stage, STAGE::PRE, STAGE::POST);

				glLinkProgram(program->getProgramID());

				utility::checkProgramLog(program->getProgramID(), GL_LINK_STATUS);
				utility::GL_errorCheck ("ShaderHandler :POST:");

			break;

			case KEYWORDS::END:
				stage = checkAndIncrementStage(stage, STAGE::POST, STAGE::END);

				fileToID[path] = program->getProgramID();
				IDtoShaderProgram[program->getProgramID()] = program;
				shaderPrograms.push_back(program);

				fileStream.close();
				
				glUseProgram(program->getProgramID());
				utility::GL_errorCheck ("ShaderHandler :END:");

				return program;
			break;

			case KEYWORDS::VS:
				if (checkStage(stage, STAGE::START)) {
					program->setShader(loadShader(data, SHADERTYPE::VERTEX)->getShaderID(), SHADERTYPE::VERTEX);
				}
			break;

			case KEYWORDS::FS:
				if (checkStage(stage, STAGE::START)) {
					program->setShader(loadShader(data, SHADERTYPE::FRAGMENT)->getShaderID(), SHADERTYPE::FRAGMENT);
				}
			break;

			case KEYWORDS::BFDL:
				if (checkStage(stage, STAGE::PRE)) {
					glBindFragDataLocation(
						program->getProgramID(),
						std::stoi(data.substr(0, data.find_first_of(' '))),
						data.substr(data.find_first_of(' ') + 1).c_str()
					);

					utility::GL_errorCheck ("ShaderHandler :BFDL:");
				}
			break;

			case KEYWORDS::GAL:
				if (checkStage(stage, STAGE::POST)) {
					program->createAttributeRefrence(data);
					utility::GL_errorCheck ("ShaderHandler :GAL:");
				}
			break;

			case KEYWORDS::GUL:
				if (checkStage(stage, STAGE::POST)) {
					program->createUniformRefrence(data);
					utility::GL_errorCheck ("ShaderHandler :GUL:");
				}
			break;

			case KEYWORDS::UNDEFINED:
				fileStream.close();
				return NULL;
			break;
		}

		if (stage == STAGE::ERROR) {

			fileStream.close();
			return NULL;

		} else {

			std::getline(fileStream, line);
			keyword = parseKeyword(line.substr(0, line.find_last_of(":") + 1));
			data = (line.find_last_of(":") + 2 < line.length()) ? line.substr(line.find_last_of(":") + 2) : "";

		}
	}

	fileStream.close();
	return NULL;
}

Shader * ShaderHandler::loadShader (const std::string &path, SHADERTYPE type) {
	Shader * shade = new Shader(utility::loadCompileShader(path, (GLenum) type), type);
	IDtoShader[shade->getShaderID()] = shade;
	shaders.push_back(shade);

	return shade;
}