#include "Basic3Ddata.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>


Basic3Ddata::Basic3Ddata () :
	Basic3Ddata (glm::vec3 (), glm::quat (), glm::vec3 (1.0f, 1.0f, 1.0f), glm::vec3 ()) {
}

Basic3Ddata::Basic3Ddata (glm::vec3 position, glm::quat rotation) :
	Basic3Ddata (position, rotation, glm::vec3 (1.0f, 1.0f, 1.0f), glm::vec3 ()) {
}

Basic3Ddata::Basic3Ddata (glm::vec3 position, glm::quat rotation, glm::vec3 scale) :
	Basic3Ddata(position, rotation, scale, glm::vec3 ()) {
}

Basic3Ddata::Basic3Ddata (glm::vec3 position, glm::quat rotation, glm::vec3 scale, glm::vec3 offset) :
	position (position), rotation (rotation), scale (scale), offset (offset) {
}



void Basic3Ddata::setPosition (glm::vec3 newPosition) {
	this->position = newPosition;
}

void Basic3Ddata::relativPosition (glm::vec3 addPosition) {
	this->position += addPosition;
}

glm::vec3 Basic3Ddata::getPosition () const {
	return this->position;
}


void Basic3Ddata::setOffset (glm::vec3 newOffset) {
	this->offset = newOffset;
}

void Basic3Ddata::relativOffset (glm::vec3 addOffset) {
	this->offset += addOffset;
}

glm::vec3 Basic3Ddata::getOffset () const {
	return this->offset;
}


void Basic3Ddata::setScale (glm::vec3 newScale) {
	this->scale = newScale;
}

void Basic3Ddata::relativScale (glm::vec3 addScale) {
	this->scale += addScale;
}

glm::vec3 Basic3Ddata::getScale () const {
	return this->scale;
}



void Basic3Ddata::setRotation (glm::quat newRotation) {
	this->rotation = newRotation;
}

void Basic3Ddata::relativRotation (glm::quat addRotation) {
	this->rotation = addRotation * rotation;
}

glm::quat Basic3Ddata::getRotation () const {
	return this->rotation;
}


glm::mat4 Basic3Ddata::getMatrix () {
	glm::mat4 modelMatrix;//= glm::mat4_cast(rotation);

	// scale first, offset is relative to the models so must be scaled
	modelMatrix = glm::translate (modelMatrix, offset) * glm::scale (modelMatrix, scale);

	modelMatrix = glm::mat4_cast (rotation) * glm::translate (modelMatrix, position);

	//modelMatrix = ;

	return modelMatrix;
}