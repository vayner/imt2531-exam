#include "Palette.h"

Palette::Palette (glm::vec2 position, glm::vec2 screenSize, glm::vec2 size, glm::vec2 lastPickSize) :
Palette (position, screenSize, size) {

	lastHistory = true;
	this->lastPickSize = lastPickSize;

	int n = size.x / lastPickSize.x;
	
	for (size_t i = 0; i < n; i++) {
		lastPicks.push_back (
			new GuiRectangle (
				glm::vec2 (position.x + i * lastPickSize.x, position.y + size.y),
				screenSize,
				lastPickSize,
				glm::vec3 (1.0f,1.0f,1.0f)
			)
		);
	}
}

Palette::Palette (glm::vec2 position, glm::vec2 screenSize, glm::vec2 size) :
GuiElement (position, screenSize, size, glm::vec3 (1.0f, 1.0f, 1.0f)), lastPickSize (glm::vec2 (0.0f, 0.0f)) {
	this->loadTexture ("textures/RGB_24bits_palette_color_chart_cropped.png", GL_BGR, GL_RGB);
}

Palette::~Palette () {
	for (auto &lp : lastPicks) {
		delete lp;
	}
}

glm::vec3 Palette::pickColor (int x, int y) {

	if (this->hitTest (x, y)) {
		Uint8 pixelData[3];
		glReadPixels (x, screenSize.y - y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixelData);

		glm::vec3 colorPicked = (glm::vec3 (pixelData[0], pixelData[1], pixelData[2]) / 255.0f);
		if (isDragSelecting) {
			isDragSelecting = false;
		} else if (y > position.y + size.y) {

			lastPicks.at ((x - position.x) / lastPickSize.x)->setColor (lastPicks.at (0)->getColor ());
			lastPicks.at (0)->setColor (colorPicked);

		} else {
			glm::vec3 currentColor = colorPicked;
			glm::vec3 lastColor = colorPicked;

			for (auto &lp : lastPicks) {
				currentColor = lp->getColor ();
				lp->setColor (lastColor);
				lastColor = currentColor;
			}
		}


		return colorPicked;
	}

	return (lastHistory) ? lastPicks.at (0)->getColor() : glm::vec3 (0.0f, 0.0f, 0.0f);
}

glm::vec3 Palette::startDragColor (int x, int y) {
	if (this->hitTest (x, y)) {
		Uint8 pixelData[3];
		glReadPixels (x, screenSize.y - y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixelData);

		glm::vec3 colorPicked = (glm::vec3 (pixelData[0], pixelData[1], pixelData[2]) / 255.0f);

		lastPicks.at (0)->setColor (colorPicked);

		if (isDragSelecting == false) {
			isDragSelecting = true;
			glm::vec3 currentColor = colorPicked;
			glm::vec3 lastColor = colorPicked;

			for (auto &lp : lastPicks) {
				currentColor = lp->getColor ();
				lp->setColor (lastColor);
				lastColor = currentColor;
			}
		}

		return colorPicked;
	}

	return (lastHistory) ? lastPicks.at (0)->getColor () : glm::vec3 (0.0f, 0.0f, 0.0f);
}

bool Palette::hitTest (int x, int y) {
	return (
		x > position.x &&
		x < position.x + size.x &&
		y > position.y &&
		y < position.y + size.y + lastPickSize.y
		);
}

void Palette::init (ShaderHandler * shaderSystem) {
	GuiElement::init (shaderSystem);

	for (auto &lp : lastPicks) {
		lp->init (shaderSystem);
	}
}

void Palette::draw () {
	GuiElement::draw ();

	for (auto &lp : lastPicks) {
		lp->updateUniformData ();
		lp->draw ();
	}
}