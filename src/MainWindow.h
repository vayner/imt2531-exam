#pragma once

#include "SDLHeaders.h"
#include <vector>
#include <string>

#include "GLWindow.h"

#include "ShaderHandler.h"
#include "Camera.h"

#include "Object.h"
#include "GuiElement.h"
#include "Palette.h"

//pre declarations
struct SDL_MouseMotionEvent;

// Represents a point light
struct Light {
    glm::vec3 position;
    glm::vec3 intensities; //a.k.a. the color of the light
};

class MainWindow : public GLWindow {
public:
	MainWindow (const std::string &windowName, const std::string &modelPath, ShaderHandler * shaderHandler);
	~MainWindow ();

	bool isValid () const;
	bool init ();
	void onActivate ();
	//void exec ();

private:

	void initLight ();
	void initObjects (const std::string &modelPath);
	
	void updateLight ();

	void keyboardDown (SDL_Keysym keyData);
	void keyboardUp (SDL_Keysym keyData);
	void update (double deltaTime);
	void draw ();
	void resizeWindow (int width, int height);
	void mouseMove (const SDL_MouseMotionEvent &motion);
	void mouseDown (const SDL_MouseButtonEvent &click);
	void mouseUp (const SDL_MouseButtonEvent &click);

	//SDL_Window* window;
	//SDL_GLContext glContext;

	bool valid;

	bool colorDraging = false;
	bool brushPainting = false;

	bool drift = true;
	bool spinnFigure = false;
	bool lighting = true;
	bool zooming = false;
	bool orbiting = false;

	float cameraAngleX = 0.0f;
	float cameraAngleY = 0.0f;
	float cameraZoom = 8.0f;

	Object * model;
	Palette * palette;
	GuiRectangle * colorDisplay;
	GuiElement * resetButton;
	GuiElement * undoButton;
	GuiElement * redoButton;
	GuiElement * saveButton;

	std::vector<Gui*> drawInterface;
	glm::vec3 selectedColor = glm::vec3(1.0f, 0.0f, 0.0f);

	std::string modelPath;
	glm::mat4 proj;

	Light light;
	glm::vec3 ambient;

	Camera * camera;
	ShaderHandler * shaderHandler;
	ShaderProgram * programRef;
};
